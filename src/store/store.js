import {applyMiddleware, compose, createStore} from 'redux';
import appReducer from './reducers/reducers';
import thunk from 'redux-thunk';
import {createReactNavigationReduxMiddleware} from "react-navigation-redux-helpers";

const middleware = createReactNavigationReduxMiddleware(
    state => state.nav,
);

const store = createStore(appReducer, applyMiddleware(middleware,thunk));

export default store;
