import axios from "../../myaxios";
import {finishLoading, startLoading} from "./loading.action";
import AsyncStorage from "@react-native-community/async-storage";

export const SET_TOKEN = '[USER] SET_TOKEN';
export const LOGOUT = '[USER] LOGOUT';
export const GET_USER = '[USER] GET_USER';
// export const SET_USER_DATA = 'SET_USER_DATA';

export const getUser = () => {
    return dispatch => {
        dispatch(startLoading());
        axios.get("/profile", ).then(res=> {
            dispatch(finishLoading());
            dispatch({type: GET_USER, data: res.data.result});
        })
    }
};

export const logout = () => {
    return dispatch => {
        // todo put loading
        // axios.get("/logoutLog").then(res=> {
            AsyncStorage.removeItem('sps:token');
            dispatch({type: LOGOUT});
        // })
    }
};

export const setToken = (data) => {
    return {type: SET_TOKEN, data}
};
export const setUser = (data) => {
    return {type: GET_USER, data}
};
