import {OPEN_DIALOG} from "./dialogs.action";

export const DELETE_CONFIRMATION = "[SPS] DELETE_CONFIRMATION";

export const HIDE_MESSAGE = '[MESSAGE] CLOSE';
export const SHOW_MESSAGE = '[MESSAGE] SHOW';

export const deleteConfirm = (requestLink, id, title) => {
    return (dispatch) => {
        dispatch ({
            type   : DELETE_CONFIRMATION,
            requestLink, id, title
        });
        dispatch({
            type   : OPEN_DIALOG,
            dialog: "deleteConfirmation"
        })
    };
};

// export function hideMessage()
// {
//     return {
//         type: HIDE_MESSAGE
//     }
// }
//
// export function showMessage(options)
// {
//     return {
//         type: SHOW_MESSAGE,
//         options
//     }
// }

