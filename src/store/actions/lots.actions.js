import axios from "../../myaxios";
export const GET = "[LOTS] GET";

export const getLots = (params) => {
    return dispatch => {
        axios.get("/lots/search", params).then(res=> {
            // if code has sent
            dispatch({type: GET, payload:res.data});
        })
    };
};
