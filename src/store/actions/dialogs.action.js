export const OPEN_DIALOG = "OPEN_DIALOG";
export const CLOSE_DIALOG = "CLOSE_DIALOG";
export const SET_IMAGE_FULL = "SET_IMAGE_FULL";


export const openDialog = (dialog) => {
    return {
        type   : OPEN_DIALOG,
        dialog
    };
};
export const closeDialog = (dialog) => {
    return {
        type   : CLOSE_DIALOG,
        dialog
    };
};
export const setFullImage = (title, src) => {
    return {
        type   : SET_IMAGE_FULL,
        title, src
    };
};