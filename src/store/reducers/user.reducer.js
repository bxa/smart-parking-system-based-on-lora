import * as Actions from '../actions/actions'

const initialState = {
    user: {},
    token: null,
    auth: false,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case Actions.LOGOUT : {
            return {
                initialState
            }
        }
        case Actions.SET_TOKEN : {
            return {
                ...state,
                token: action.data,
                auth: true
            }
        }
        case Actions.GET_USER : {
            return {
                ...state,
                user: action.data
            }
        }
        default: return state;
    }
};

export default reducer;
