import * as Actions from '../actions/actions'

const initialState = {
    metadata: {}, result: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case Actions.GET : return {...action.payload};
        default: return state;
    }
};

export default reducer;
