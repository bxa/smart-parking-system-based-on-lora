import * as Actions from '../actions/actions';

const initialState = {
    state  : null,
    options: {
        // anchorOrigin    : {
        //     vertical  : 'bottom',
        //     horizontal: 'left'
        // },
        color           : "successDark",
        autoHideDuration: 5000,
        message         : "Hi",
        parameter       : null,
    }
};

const message = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.SHOW_MESSAGE:
        {
            return {
                ...state,
                state  : true,
                options: {
                    ...state.options,
                    // anchorOrigin    : {
                    //     ...state.options.anchorOrigin,
                    //     ...action.options.anchorOrigin
                    // },
                    ...action.options
                }
            };
        }
        case Actions.HIDE_MESSAGE:
        {
            return {
                ...state,
                state: null
            };
        }
        default:
        {
            return state;
        }
    }
};

export default message;
