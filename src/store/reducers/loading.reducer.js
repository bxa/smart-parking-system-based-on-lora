import * as Actions from '../actions/actions';

const loading = function (state = false, action) {
    switch ( action.type ) {
        case Actions.START_LOADING: return true;
        case Actions.FINISH_LOADING: return false;
        default: {
            return state;
        }
    }
};

export default loading;
