import * as Actions from '../actions/actions';

const dialogs = function (state = {}, action) {
    switch ( action.type )
    {
        case Actions.OPEN_DIALOG: {
            return {
                ...state,
                [action.dialog]: true
            }
        }
        case Actions.CLOSE_DIALOG: {
            return {
                ...state,
                [action.dialog]: false
            }
        }
        case Actions.SET_IMAGE_FULL: {
            return {
                ...state,
                selectedImage: {
                    title :   action.title,
                    src :   action.src,
                }
            }
        }

        default:
        {
            return state;
        }
    }
};

export default dialogs;
