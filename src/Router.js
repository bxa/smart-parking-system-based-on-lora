import React, {useContext } from "react";
import {Text, View, Alert, Modal, TouchableHighlight, Switch, Platform} from "react-native";
import {
    createAppContainer,
    createSwitchNavigator
} from "react-navigation";

// import profileRoutes from "./app/Profile/profileRoutes";
import Login from "./app/Login/Login";
import ScreenSplash from "./app/Login/ScreenSplash";
import Notifications from "./app/Notifications/Notifications";
import {ThemeContext} from "styled-components";
import Profile from "./app/Profile/Profile";
import Icon from "react-native-vector-icons/MaterialIcons";
import {BottomTabBar, createBottomTabNavigator} from "react-navigation-tabs";
import {createStackNavigator} from "react-navigation-stack";
import profileRoutes from "./app/Profile/profileRoutes";
import Homepage from "./app/home/Homepage";
import Explore from "./app/explore/Explore";
import SingleLot from "./app/lots/SingleLot";
import StartTime from "./app/orders/StartTime";
import lotRouters from "./app/lots/lotRoutes";
import ScanQrCode from "./app/home/ScanQrCode";

const TabBarComponent = (props) => {
    const themeContext = useContext(ThemeContext);
    return (<BottomTabBar {...props}  activeTintColor={themeContext.colors.primary} style={{backgroundColor: themeContext.colors.bg, borderTopWidth: 0}}/>)
};




const AuthRoutes = createStackNavigator({
    login: {
        screen: Login,
        navigationOptions: {
            header: null
        }
    },
});

const mainBottomRoutes = createBottomTabNavigator({
        Home : {
            screen: Homepage,
            navigationOptions: {
                tabBarIcon: ({ focused, horizontal, tintColor }) => <Icon color={tintColor} size={28}  name={"home"} outlined/>
            }
        },
        Explore : {
            screen: Explore,
            navigationOptions: {
                tabBarIcon: ({ focused, horizontal, tintColor }) => <Icon color={tintColor} size={28}  name={"explore"} />
            }
        },
        Explores : {
            screen: Explore,
            navigationOptions: {
                tabBarIcon: ({ focused, horizontal, tintColor }) => <Icon color={tintColor} size={60}  name={"time-to-leave"} />
            }
        },
        Notifications : {
            screen: Notifications,
            navigationOptions: {
                tabBarIcon: ({ focused, horizontal, tintColor }) => <Icon color={tintColor} size={28}  name={"notifications-none"}   />
            }
        },
        Profile : {
            screen: Profile,
            navigationOptions: {
                tabBarIcon: ({ focused, horizontal, tintColor }) => <Icon color={tintColor} size={28}  name={"account-circle"} />
            }
        }
    }
    ,{
        navigationOptions: ({ navigation }) => {
            return {
                header: null
            }
        },
        tabBarComponent: props =>
            <TabBarComponent
                {...props}
            />,
        tabBarOptions : {
            style : {
                backgroundColor: "transparent",
            },
            tabStyle: {
                backgroundColor: "transparent"
            },
            // safeAreaInset: { top: 'always', bottom: 'never' },
            showLabel: false,
            keyboardHidesTabBar: true
        }
    }
);

const mainRoutes = {
    profile : profileRoutes,
    lot     : lotRouters,
};

const AppRoutes = createStackNavigator({
    home: mainBottomRoutes,
    ScanQR: {screen : ScanQrCode, navigationOptions: {
        header: null
        }},
    ...mainRoutes
});

const RouterNav = createSwitchNavigator({
    AuthLoading: { screen: ScreenSplash},
    Auth: AuthRoutes,
    App: AppRoutes,
}, {
    initialRouteName: 'AuthLoading',
});

export default createAppContainer(RouterNav);
