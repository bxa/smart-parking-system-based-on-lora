import React from "react";
import {
    View,
    StyleSheet,
} from "react-native";
import styled from "styled-components";
import PropTypes from "prop-types";
import {withTheme} from "styled-components";

const BaseDivider = styled.View`
    border-bottom-width: ${StyleSheet.hairlineWidth};
    border-bottom-color: ${({theme})=>theme.colors.border};
`;

const Divider = props => {
    return <BaseDivider {...props} />

};

Divider.propTypes = {
    small:  PropTypes.bool,
};

export default withTheme(Divider);
