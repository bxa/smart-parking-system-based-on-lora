import React from "react";
import {
    TouchableOpacity,
    TouchableNativeFeedback,
    Text,
    View,
    StyleSheet,
    Platform
} from "react-native";
import styled from "styled-components";
import Icon from "react-native-vector-icons/Ionicons";
import PropTypes from "prop-types";
import P from "./P";
import {withTheme} from "styled-components";

// const TabsInner = (props) => {
//     if(!props.disabled) {
//         // if (Platform.OS === "android") return (<TouchableNativeFeedback {...props}/>);
//         return (<TouchableOpacity {...props}/>);
//     }
//     else return (<View {...props}/>)
// };

const BaseTabs = styled.View`
    border-bottom-width: ${StyleSheet.hairlineWidth} ;
    border-bottom-color: ${({theme}) => theme.colors.border};
    flex-direction: row;
    align-items: center;
`;

const Tabs = props => {
    return (
        <BaseTabs {...props}/>
    )
};

export default withTheme(Tabs);
