import React, {Component} from "react";
import {ScrollView, View, Text,RefreshControl} from "react-native";
import styled from "styled-components";
import {withTheme} from "styled-components";
import * as Actions from "../store/actions/actions";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import PropTypes from "prop-types";
// import {useDispatch} from "react-redux";
// import * as Actions from "../store/actions/srx.actions";
// import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';

const Styled = styled.ScrollView`
  background-color: ${props => props.theme.colors.bg};
  flex: 1;
  margin-bottom: ${(props)=> props.hasActions ? props.theme.margins.side*3 : 0};
`;

class Container extends Component {
    // state = {
    //     translateY: 0,
    //     position: "bottom"
    // };
    // constructor(props) {
    //     super(props);
    //     this.myRef = React.createRef();
    // }
    // onScroll = (e)=>{
    //     this.props.scrolling(e.nativeEvent.contentOffset.y);
    //     // console.log(e.nativeEvent.contentOffset.y);
    // };
    // onMomentumScrollEnd = (e) => {
    //     if (e.nativeEvent.contentOffset.y < 10) {
    //         this.myRef.scrollTo({x: 0, y: 0, animated: true})
    //     } else if (e.nativeEvent.contentOffset.y > 10 && e.nativeEvent.contentOffset.y < 200) {
    //         this.myRef.scrollTo({x: 0, y: 200, animated: true})
    //     }
    //     this.props.scrolled(true);
    // };

    //     this._panResponder = PanResponder.create({
    //         // Ask to be the responder:
    //         // onStartShouldSetPanResponder: (evt, gestureState) => {
    //         //     console.log(gestureState.dy);
    //         //     return true
    //         // },
    //         // onStartShouldSetPanResponderCapture: (evt, gestureState) => {
    //         //     // console.log(gestureState.dy);
    //         //     return true
    //         // },
    //         // onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
    //         onMoveShouldSetPanResponder: (evt, gestureState) => true,
    //         onMoveShouldSetPanResponderCapture: (evt, gestureState) => {
    //                 if(gestureState.dy > 2 || gestureState.dy < -2)
    //                     return true;
    //                 else return false
    //             },
    //         //
    //         // onPanResponderGrant: (evt, gestureState) => {
    //         //     console.log(gestureState)
    //         //     // The gesture has started. Show visual feedback so the user knows
    //         //     // what is happening!
    //         //     // gestureState.d{x,y} will be set to zero now
    //         // },
    //         onPanResponderMove: (evt, gestureState) => {
    //             // console.log(gestureState.dy);
    //
    //             // this.props.scrolling(gestureState.dy);
    //             // The most recent move distance is gestureState.move{X,Y}
    //             // The accumulated gesture distance since becoming responder is
    //             // gestureState.d{x,y}
    //         },
    //         onPanResponderTerminationRequest: (evt, gestureState) => true,
    //         onPanResponderRelease: (evt, gestureState) => {
    //             // this.props.scrolled(true);
    //             // The user has released all touches while this view is the
    //             // responder. This typically means a gesture has succeeded
    //         },
    //         onPanResponderTerminate: (evt, gestureState) => {
    //             console.log(gestureState);
    //             // Another component has become the responder, so this gesture
    //             // should be cancelled
    //         },
    //         // onShouldBlockNativeResponder: (evt, gestureState) => {
    //         //     // Returns whether this component should block native components from becoming the JS
    //         //     // responder. Returns true by default. Is currently only supported on android.
    //         //     return true;
    //         // },
    //     });
    // }

    render() {
        return <Styled
            keyboardShouldPersistTaps={"handled"}
            refreshControl={ this.props.onRefresh &&
                <RefreshControl
                    refreshing={this.props.loading}
                    onRefresh={this.props.onRefresh}
                />
            }
            nestedScrollEnabled {...this.props}
        >
            {this.props.children}
        </Styled>;
    }
}


Container.propTypes = {
    hasActions:  PropTypes.bool,
    onRefresh:  PropTypes.func,
};


function mapStateToProps({task, loading}) {
    return {
        loading,
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        scrolling    : Actions.scrolling,
        scrolled     : Actions.scrolled
    }, dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(withTheme(Container));
