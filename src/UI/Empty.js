import React from "react";
import {
    View,
    ActivityIndicator,
} from "react-native";
import {useSelector} from "react-redux";
import {withTheme} from "styled-components";
import Icon from "react-native-vector-icons/MaterialIcons";
import P from "./P";

const Empty = ({theme}) => {
    const loading = useSelector(({loading}) => loading);

    return (
        <View style={{marginTop: 40, marginBottom: 40, alignItems: "center"}}>
            {loading ? <ActivityIndicator size={50} color={theme.colors.textLight}/> :
                <>
                    <Icon name={"plumbing"} size={40} color={theme.colors.textLight} style={{marginBottom: 20}}/>
                    <P type={"h5"} color={"textLight"} align={"center"}>Sorry, We can't find anything right now.</P>
                </>
            }
        </View>
    )
};

export default withTheme(Empty);
