import React from "react";
import {View, ScrollView, Dimensions} from "react-native";
import {withTheme} from "styled-components";
import styled from "styled-components";
import PropTypes from 'prop-types';

const {height, width} = Dimensions.get('window');

const Styled = styled.View`
    background-color: ${props => props.theme.colors.bgLight};
    margin-right: ${props => props.theme.margins.side};
    margin-left: ${props => props.theme.margins.side}; 
    margin-bottom: ${props => props.noMargin ? 0 :props.theme.margins.side*2}; 
    border-radius: ${props => props.theme.baseRadius};
    overflow: hidden;
    flex: ${props => props.noFlex ? "none" : 1};
    padding: ${props=> props.hasPadding ? props.theme.margins.side*2 : 0}px;
`;

const Card = (props) => {
    return (
        <Styled {...props}/>
    )
};

Card.propTypes = {
    hasPadding: PropTypes.bool,
    noFlex: PropTypes.bool,
    noMargin: PropTypes.bool,
};

export default withTheme(Card);

