import React from "react";
import {Switch, View, FlatList} from "react-native";
import P from "./P";
import styled, {withTheme} from "styled-components";


const Styled = styled.View`
    padding: ${({theme}) => theme.margins.side * 2}px;
`;

const CheckboxGroup = props => {
    // let error = null;
    // if(e.rawErrors) {
    //     error = e.rawErrors[0];
    // }

    const onChangeHandler = (event, item) => {
        if (event) {
            const v = [...props.value];
            v.push(item);
            props.onChange(v)
        } else {
            props.onChange(props.value.filter(i => i !== item));
        }
    };

    return (
        <Styled
            error={props.error !== null && props.error !== undefined}
            // required={props.schema.require}
            fullWidth
            disabled={props.disabled}>
            <P color={"textLight"}>{props.title}</P>
            <FlatList
                listKey={props.id}
                numColumns={2}
                extraData={props}
                data={props.items}
                renderItem={({item}) =>
                    <View
                        style={{
                            flexDirection: "row",
                            flex: 1,
                            alignItems: "center",
                            paddingTop: 10,
                            paddingBottom: 10
                        }}>
                        <Switch
                            trackColor={{true: props.theme.colors.primary, false: props.theme.colors.textLight}}
                            value={props.value.includes(item)}
                            onValueChange={(val) => onChangeHandler(val, item)}
                            disabled={props.disabled}
                            label={item}
                        />
                        <P>{item}</P>
                    </View>
                }
            />
            {props.helper !== undefined ? <P color={"textLight"}>{props.helper}</P> : null}
        </Styled>
    )
};

export default withTheme(CheckboxGroup)
