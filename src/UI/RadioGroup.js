import React from "react";
import {Switch, View, FlatList} from "react-native";
import P from "./P";
import styled, {withTheme} from "styled-components";
import PropTypes from "prop-types";

const Styled = styled.View`
    padding: ${({theme})=> theme.margins.side*2}px;
`;

const RadioGroup = props => {
    // let error = null;
    // if(e.rawErrors) {
    //     error = e.rawErrors[0];
    // }
    return (
        <Styled
            error={props.error !== null && props.error !== undefined}
            // required={props.schema.require}
            fullWidth
            disabled={props.disabled}>
            <P color={"textLight"}>{props.title}</P>
            <FlatList
                listKey={props.id}
                numColumns={2}
                extraData={props}
                data={props.items}
                renderItem={({item})=> <View
                          style={{flexDirection: "row", flex:1, alignItems: "center", paddingTop: 10, paddingBottom: 10}}>
                        <Switch
                            trackColor={{true: props.theme.colors.primary, false: props.theme.colors.textLight}}
                            value={props.value === item}
                            onValueChange={() => props.onChange(props.value === item ? null : item)}
                            disabled={props.disabled}
                            label={item}
                        />
                        <P>{item}</P>
                    </View>
                }
            />
            {props.helper !== undefined ? <P color={"textLight"}>{props.helper}</P> : null}
        </Styled>
    )
};

RadioGroup.propTypes = {
    error      : PropTypes.string,
    title      : PropTypes.string,
    helper     : PropTypes.string,
    disabled   : PropTypes.bool,
    items      : PropTypes.array,
    value      : PropTypes.string,
    onChange   : PropTypes.func
};

export default withTheme(RadioGroup)
