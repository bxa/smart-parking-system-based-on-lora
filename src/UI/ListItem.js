import React from "react";
import {View, TouchableOpacity, StyleSheet} from "react-native";
import {withTheme} from "styled-components";
import styled from "styled-components";
import PropTypes from 'prop-types';
import P from "./P";

// ios-fix
const ListItemInner = (props) => {
    if (props.button) return (<TouchableOpacity {...props}/>);
    else return (<View {...props}/>)
};

const Styled = styled(ListItemInner)`
    background-color: ${props => props.transparent ? "transparent" : props.theme.colors.bgLight};
    padding-left: ${props => props.theme.margins.side * 2};
    padding-right: ${props => props.theme.margins.side * 2};
    flex-direction: row;
    align-items: center
`;

const Avatar = styled.View`
    margin-right: ${props => props.theme.margins.side * 2};
    border-radius: 100px;
    overflow: hidden;
    justify-content: center;
    align-items: center
`;

const Content = styled.View`
    padding-top: ${props => props.theme.margins.side * 1.5};
    padding-bottom: ${props => props.theme.margins.side * 1.5};
    align-items: center;
    justify-content: space-between;
    flex-direction: row;
    flex: 1;
    border-bottom-width: ${props => props.noBorder ? 0 : StyleSheet.hairlineWidth};
    border-bottom-color: ${props => props.noBorder ? props.theme.colors.bg : props.theme.colors.border};
`;

const ListItem = (props) => {
    return (
        <>
            <Styled {...props}>
                {props.avatar && <Avatar theme={props.theme}>
                    {props.avatar}
                </Avatar>}
                <Content noBorder={props.noBorder} theme={props.theme}>
                    <View>
                        {props.titleComponent || <P color={props.titleColor || "text"} type={props.titleType || "h6"}>{props.title}</P>}
                        {props.subtitleComponent || (props.subtitle ? <P color={"textLight"} style={{marginTop: 3}}>{props.subtitle}</P> : null)}
                    </View>
                    <View>
                        {props.actions}
                    </View>
                </Content>
            </Styled>
            {props.children}
        </>
    )
};

ListItem.propTypes = {
    titleType: PropTypes.string,
    button: PropTypes.bool,
    transparent: PropTypes.bool,
    noBorder: PropTypes.bool,
    title: PropTypes.string,
    titleColor: PropTypes.string,
    subtitle: PropTypes.string,
    actions: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.func,
    ]),
    titleComponent: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.func,
    ]),
    subtitleComponent: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.func,
    ]),
    avatar: PropTypes.element,
};

export default withTheme(ListItem);

