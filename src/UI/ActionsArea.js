import React, {useState} from "react";
import {View} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import styled, {withTheme} from "styled-components";
import {NavigationActions} from "react-navigation";
import PropTypes from "prop-types";
import Button from "./Button";

const Styled = styled.View`
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 50px;
    background-color: ${(props)=> props.light ? props.theme.colors.bgLight: props.theme.colors.bg};    
    flex-direction: row;
    align-items: center;
    justify-content: space-around ;
`;

const ActionsArea = (props) => {
    const loading = useSelector(({loading}) => loading);
    const dispatch = useDispatch();

    return (
        <Styled {...props}>
            {!props.disableCancel && <Button onPress={()=>dispatch(NavigationActions.back())}>Cancel</Button>}
            {props.children}
        </Styled>
    )
};


ActionsArea.propTypes = {
    disableCancel: PropTypes.bool,
    light: PropTypes.bool,
};

export default withTheme(ActionsArea);
