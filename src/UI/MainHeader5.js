import React, {Component, useEffect, useState} from "react";
import {Text, View, Alert, Modal, TouchableHighlight, Switch, StyleSheet} from "react-native";
import {withNavigation} from 'react-navigation';
import {connect, useDispatch, useSelector} from "react-redux";
import {withTheme} from "styled-components";
import P from "./P";
import * as Actions from "../store/actions/actions";
import {bindActionCreators} from "redux";

const mainHeight = 250;

class MainHeader5 extends Component {
    state = {
        height: mainHeight,
        k: mainHeight,
        position: "bottom"
    };

    componentDidUpdate = (prevProps, prevState) => {
        const {y, scrolledState} = this.props;
        const {height, k, position} = this.state;
        // console.log(height, k, position, y, scrolledState);
        if (prevProps.y !== y) {
            if (!scrolledState && !prevProps.scrolledState) {
                if (y < (mainHeight-60) && y > 0 &&  position === "top") { // swipe down
                         this.setState({height: k + y});
                }
                if (y > -(mainHeight-60) && y < 0 &&  position === "bottom") { // swipe up
                         this.setState({height: k + y});
                }

            }
        }
        if((prevProps.scrolledState !== scrolledState && scrolledState)
            && !(position === "bottom" && y < (mainHeight-60) && y > 0)
            && !(position === "top" && y > -(mainHeight-60) && y < 0)
            ) {
            if(position === "bottom" && y < (mainHeight-60) && y > 0) {
                console.log("youre swiping down for nothing");
            }
            if(position === "top" && y > -(mainHeight-60) && y < 0) {
                console.log("youre swiping up for nothing");
            }
            if (height > 150) {
                this.setState({position: "bottom", height: mainHeight, k: mainHeight})
            } else if (height < 150) {
                this.setState({position: "top", height: 60, k: 60})
            }
            this.props.scrolled(false)
        }
        if(prevProps.scrolledState !== scrolledState && scrolledState) {
            this.props.scrolled(false)
        }
    };

    render() {
        const {height} = this.state;
        return (
            <View style={{height}}>
                <View style={{alignItems: "center", justifyContent: "center", flex: 1,opacity: (height-60)/(mainHeight - 60)}}>
                    {this.props.children || <View>
                        <P type={"h1"} align={"center"}>{this.props.title}</P>
                        <P type={"h6"} align={"center"}>{this.props.subTitle}</P>
                    </View>}
                </View>
                <View>{this.props.actions}</View>
            </View>
        )
    }
};

function mapStateToProps({srx}) {
    return {
        y: srx.y,
        scrolledState: srx.scrolled
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        scrolled: Actions.scrolled
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(withNavigation(MainHeader5)));
