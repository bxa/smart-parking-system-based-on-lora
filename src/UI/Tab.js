import React from "react";
import {
    TouchableOpacity,
    TouchableNativeFeedback,
    Text,
    View,
    StyleSheet,
    Platform
} from "react-native";
import styled from "styled-components";
import Icon from "react-native-vector-icons/Ionicons";
import PropTypes from "prop-types";
import P from "./P";
import {withTheme} from "styled-components";

const TabInner = (props) => {
    if(!props.disabled && !props.active) {
        // if (Platform.OS === "android") return (<TouchableNativeFeedback {...props}/>);
        return (<TouchableOpacity {...props}/>);
    }
    else return (<View {...props}/>)
};

const BaseTab = styled(TabInner)`
    padding: ${props => props.theme.margins.side}px;
    align-items: center;
    flex: 1;
`;

const Tab = props => {
    return (
        <BaseTab {...props}>
            {props.icon && <Icon style={{margin:5}} color={props.active ? props.theme.colors.primary : props.theme.colors.text} size={26} name={(Platform.OS === "android") ? "ios-"+props.icon : "md-"+props.icon} />}
            <P style={{marginBottom: 5}} align={"center"} color={props.active ?  "primary" : "text"}>
                {props.children}
            </P>
        </BaseTab>
    )

};

Tab.propTypes = {
    disabled: PropTypes.bool,
    active: PropTypes.bool,
    icon:  PropTypes.string,
};

export default withTheme(Tab);
