import React from "react";
import styled, {withTheme} from "styled-components";
import Button from "./Button";
import {withNavigation} from 'react-navigation';

const Styled = styled.View`
    background-color: ${props => props.theme.colors.bg};
    flex-direction: row;
    align-items: center;
`;


const BackHeader = (props) => {
    return (<Styled {...props}>
        <Button icon={"arrow-back"} onPress={()=>props.navigation.goBack()}>Back to {props.backTitle}</Button>
    </Styled>)
};



export default withNavigation(withTheme(BackHeader));
