import React from "react";
import {View, StyleSheet, Text} from "react-native";
import {withTheme} from "styled-components";
import styled from "styled-components";
import PropTypes from 'prop-types';
import IconButton from "./IconButton";

const textsColors = {
    'bg': "textLight",
    'bgLight': "textLight",
    'text': "bgLight",
    'textLight': "bgLight",
    'primary': "text",
    'border': "bgLight",
    'error': "text",
    'warning': "bgLight",
    'success': "white",
    'info': "white",
    "primaryDark" : "white",
    "errorDark" : "white",
    "warningDark" : "white",
    "successDark" : "white",
    "infoDark" : "white",
    "purple" : "white",
    "purpleDark" : "white",
    "orange" : "white",
    "orangeDark" : "white",
};

const InsideText = styled.Text`
  color: ${props => props.color ? props.theme.colors[textsColors[props.color]] : props.theme.colors.textLight};
  font-size: ${props => props.theme.typo[props.type || "p"]}
  text-align: center;
  font-weight: bold;
`;

const Wrapper = styled.View`
  margin: 5px;
  flex-direction: row;
  align-items: center;
  background-color: ${props => props.color ? props.theme.colors[props.color] : props.theme.colors.bgLight};
  border-radius: 50px;
  padding:${props => props.small ?  "2px 6px" : props.closeAction ? "3px 5px 3px 10px" :"3px 10px"};

`;

const Chip = (props) => {
    return (
        <Wrapper theme={props.theme} small={props.small} closeAction={!!props.closeAction} color={props.color}>
            <InsideText {...props}/>
            {props.closeAction && <IconButton style={{marginLeft: 5}} size={24} onPress={props.closeAction} name={"close-circle"} />}
        </Wrapper>
    )
};

Chip.propTypes = {
    color: PropTypes.oneOf([
        "primary",
        "primaryDark",
        "error",
        "warning",
        "success",
        "info",
        "errorDark",
        "warningDark",
        "successDark",
        "infoDark",
        "purple",
        "purpleDark",
        "orange",
        "orangeDark",
        "bg",
        "bgLight",
        "text",
        "textLight",
        "border",
    ]),
    closeAction: PropTypes.func,
    type: PropTypes.oneOf(['h1', 'h2', 'h3', 'h4', 'h5', 'h6', "small"]),
    small: PropTypes.bool
};

export default withTheme(Chip);

