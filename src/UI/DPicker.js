import React from "react";
import {View, StyleSheet} from "react-native";
import P from "./P";
import styled, {withTheme} from "styled-components";
import DatePicker from "react-native-datepicker";

const Styled = styled.View`
    padding: ${({theme}) => theme.margins.side * 2}px;
`;

const DPicker = props => {

    // let error = null;
    // if(e.rawErrors) {
    //     error = e.rawErrors[0];
    // }

    return (
        <Styled
            error={props.error !== null && props.error !== undefined}
            // required={props.schema.require}
            fullWidth
            disabled={props.disabled}>
            <P color={"textLight"}>{props.title}</P>
            <DatePicker
                showIcon={false}
                style={{
                    width: "100%",
                    borderBottomWidth: StyleSheet.hairlineWidth,
                    borderBottomColor: props.theme.colors.textLight
                }}

                date={props.value}
                mode={props.mode}
                placeholder="Select"
                customStyles={{
                    dateInput: {
                        borderWidth: 0,
                    },
                    dateText: {
                        fontSize: 18,
                        color: props.theme.colors.text
                    },
                    placeholderText: {
                        fontSize: 18,
                        color: props.theme.colors.textLight
                    },
                }}
                onDateChange={(val) => props.onChange(val)}
            />
            {props.helper !== undefined ? <P color={"textLight"}>{props.helper}</P> : null}
        </Styled>
    )
};

export default withTheme(DPicker)
