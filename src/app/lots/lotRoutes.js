import React, {useContext} from "react";
import styled, {ThemeContext} from "styled-components";
import Button from "../../UI/Button";
import SingleLot from "./SingleLot";
import StartTime from "../orders/StartTime";
import {createStackNavigator} from "react-navigation-stack";

const Styled = styled.View`
    background-color: ${props => props.theme.colors.bg};
    flex-direction: row;
    align-items: center;
`;

const Header = (props) => {
    const theme = useContext(ThemeContext);
    return (<Styled {...props} theme={theme}>
        <Button icon={"arrow-back"} onPress={()=>props.navigation.goBack()}>Back to profile</Button>
    </Styled>)
};

const lotRouters =
    createStackNavigator({
        lot     : {screen: SingleLot},
        timer   : {screen: StartTime}
    }, {
        navigationOptions: (props) => {
            // const themeContext = useContext(ThemeContext);
            return {
                title: "Back",
                // headerTransparent: true,
                header: <Header {...props}/>,
                // headerStyle: true,
                // headerStyle: styles.header
            }
        },
        backBehavior: "none",
    });


export default lotRouters;
