import React, {Component} from 'react';
import {View} from "react-native";
import Card from "../../UI/Card";
import ListItem from "../../UI/ListItem";
import P from "../../UI/P";
import Button from "../../UI/Button";

class Checkout extends Component {
    state = {
        price: '30.00',
        lot: "Lolo Parking lot",
        duration: '1h 3min',
        value: 'wechat',
    };

    handleChange = event => {
        this.setState({ value: event.target.value });
    };

    render() {
        const {classes} = this.props;
        return (
            <View>
                <Card>
                    <ListItem title={this.state.lot} actions={<>{this.state.duration}</>} />
                    <View>
                        <P  type={"h2"} color={"primary"}>{this.state.price}</P>
                        <P>Report a problem</P>
                    </View>
                    {/*<P>WeChat Payment</P>*/}
                    {/*<P>AliPay Payment</P>*/}
                    {/*<RadioGroup*/}
                    {/*    aria-label="gender"*/}
                    {/*    name="gender2"*/}
                    {/*    className={classes.group}*/}
                    {/*    value={this.state.value}*/}
                    {/*    onChange={this.handleChange}*/}
                    {/*>*/}
                    {/*    <FormControlLabel value="wechat" control={<Radio classes={{root: classes.root}} color="secondary" />} label="WeChat Payment" />*/}
                    {/*    <FormControlLabel value="alipay" control={<Radio color="secondary" />} label="AliPay Payment" />*/}
                    {/*</RadioGroup>*/}

                    <Button size="large" type={"contained"} fullWidth  color="primary">Pay {this.state.price}</Button>
                </Card>
            </View>
        );
    }
}

export default withStyles(styles)(Checkout);
