import React, {Component} from 'react';
import {connect} from 'react-redux';
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import {bindActionCreators} from "redux";
import * as Actions from "../../../store/actions/actions";
import axios from "../../../myaxios"
import MenuItem from "@material-ui/core/MenuItem";

class GenderDialog extends Component {

    state = {
        loading: false,
        value: this.props.user.data.gender
    };

    onSubmit = (e) => {
        e.preventDefault();
        axios.put("/profile", {gender: this.state.value}).then(res=>{
            this.props.closeDialog("changeGender");
        })
    };

    render() {
        return (
            <Dialog
                open={this.props.dialogs['changeGender']}
                onClose={() => this.props.closeDialog("changeGender")}
                aria-labelledby="form-dialog-title"
                fullWidth
                className={this.state.loading ? "loading" : ""}
            >
                <form onSubmit={this.onSubmit}>
                    <DialogTitle id="form-dialog-title">Gender</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Please enter your gender
                        </DialogContentText>
                        <TextField
                            select
                            autoFocus
                            margin="dense"
                            id="gender"
                            label="Gender"
                            fullWidth
                            value={this.state.value}
                            onChange={(e)=>this.setState({value: e.target.value})}
                        >
                            <MenuItem value={"MALE"}>
                                Male
                            </MenuItem>
                            <MenuItem  value={"FEMALE"}>
                                Female
                            </MenuItem>
                            <MenuItem  value={"NONE"}>
                                None
                            </MenuItem>
                        </TextField>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={()=>this.props.closeDialog('changeGender')}>Cancel</Button>
                        <Button type={"submit"}
                                color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        openDialog      : Actions.openDialog,
        closeDialog     : Actions.closeDialog
    }, dispatch);
}
function mapStateToProps({dialogs, user}) {
    return { dialogs, user };
}
export default connect(
    mapStateToProps,mapDispatchToProps
)(GenderDialog);
