import React, {Component} from 'react';

import {withStyles} from "@material-ui/core/styles/index";
import Card from "@material-ui/core/Card";
import CardContent from '@material-ui/core/CardContent';
import classNames from 'classnames';
import Avatar from "@material-ui/core/Avatar";
import profilePic from '../../../img/uxceo-128.jpg';
import CardHeader from "@material-ui/core/CardHeader";
import Typography from "@material-ui/core/Typography";
import Gray from "@material-ui/core/colors/grey"
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Icon from "@material-ui/core/Icon";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import {bindActionCreators} from "redux";
import * as Actions from "../../../store/actions/actions";
import {connect} from "react-redux";
import NameDialog from "./NameDialog";
import BirthdayDialog from "./BirthdayDialog";
import GenderDialog from "./GenderDialog";

const styles = theme => ({
    avatar: {
        margin: 10,
    },
    bigAvatar: {
        width: 90,
        height: 90,
        border: '5px solid rgba(0,0,0,0.2)',
        margin: 'auto',
    },
    label: {
        marginTop: 10
    },
    listItem: {
        paddingTop: 17,
        paddingBottom: 17,
        background: "white"
    },
    colorDisabled: {
        color: Gray["600"],
        fontWeight: 'lighter',
        display: 'inline-block',
        verticalAlign: 'middle',
        marginRight: 7
    },
    iconRight: {
        verticalAlign: 'middle',
    }

});

class EditProfile extends Component {
    state = {
        auth: true,
    }

    render() {
        const {classes} = this.props;
        return (
            <>
                <AppBar position="static">
                    <Toolbar disableGutters>
                        <IconButton onClick={this.props.history.goBack} color="inherit">
                            <Icon>chevron_left</Icon>
                        </IconButton>
                        <Typography variant="h6" color="inherit">
                            Edit Profile
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Card>
                    <CardHeader/>
                    <CardContent>
                        <Avatar
                            alt="Adelle Charles"
                            src={profilePic}
                            className={classNames(this.props.classes.avatar, this.props.classes.bigAvatar)}
                        />
                        <Typography   color={"textSecondary"} variant={"subtitle1"}
                                    className={this.props.classes.label}>
                            Upload Profile Picture
                        </Typography>
                    </CardContent>
                </Card>
                <List disablePadding>
                    <ListItem onClick={()=>this.props.openDialog('changeName')} button className={classes.listItem}>
                        <ListItemText classes={{primary: classes.colorDisabled}}>Name</ListItemText>
                        <ListItemSecondaryAction>
                            <Icon color={'disabled'}>chevron_right</Icon>
                        </ListItemSecondaryAction>
                    </ListItem>
                    <Divider light component={'li'}/>
                    <ListItem onClick={()=>this.props.openDialog('changeGender')} button className={classes.listItem} to="/user/payments">
                        <ListItemText classes={{primary: classes.colorDisabled}}>Gender</ListItemText>
                        <ListItemSecondaryAction>
                            <Icon color={'disabled'}>chevron_right</Icon>
                        </ListItemSecondaryAction>
                    </ListItem>
                    <Divider light component={'li'}/>
                    <ListItem  onClick={()=>this.props.openDialog('changeBirthday')}  button className={classes.listItem}>
                        <ListItemText classes={{primary: classes.colorDisabled}}>Birthday</ListItemText>
                        <ListItemSecondaryAction>
                            <Typography variant={"caption"} className={classes.colorDisabled}>H14854</Typography>
                        </ListItemSecondaryAction>
                    </ListItem>
                    <Divider light component={'li'}/>
                </List>

                <List>
                    <ListItem component={'li'} button className={classes.listItem}>
                        <ListItemText classes={{primary: classes.colorDisabled}}>Date of Register</ListItemText>
                        <ListItemSecondaryAction className={classes.flow}>
                            <Typography variant={"caption"} className={classes.colorDisabled}>01/25/2018</Typography>
                            {/*<Icon  color={'disabled'} className={classes.iconRight}>chevron_right</Icon>*/}
                        </ListItemSecondaryAction>
                    </ListItem>
                </List>

                {this.props.dialogs['changeName'] && <NameDialog/> }
                {this.props.dialogs['changeBirthday'] && <BirthdayDialog/> }
                {this.props.dialogs['changeGender'] && <GenderDialog/> }

            </>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        openDialog      : Actions.openDialog,
        closeDialog      : Actions.closeDialog
    }, dispatch);
}

function mapStateToProps({user, dialogs}) {
    return {
        user, dialogs
    }
}
export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(EditProfile));

