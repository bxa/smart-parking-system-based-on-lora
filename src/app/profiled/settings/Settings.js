import React, {Component} from 'react';

import {withStyles} from "@material-ui/core/styles/index";
import Typography from "@material-ui/core/Typography";
import Gray from "@material-ui/core/colors/grey"
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Icon from "@material-ui/core/Icon";
import Link from "react-router-dom/Link";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import axios from "../../../myaxios"

const styles = theme => ({
    avatar: {
        margin: 10,
    },
    bigAvatar: {
        width: 90,
        height: 90,
        border: '5px solid rgba(0,0,0,0.2)',
        margin: 'auto',
    },
    label: {
        marginTop: 10
    },
    listItem: {
        paddingTop: 17,
        paddingBottom: 17,
        background: "white"
    },
    colorDisabled: {
        color: Gray["600"],
        fontWeight: 'lighter',
        display: 'inline-block',
        verticalAlign: 'middle',
        marginRight: 7
    },
    iconRight: {
        verticalAlign: 'middle',
    }

});

class Settings extends Component {
    state = {
        notification: true,
    };

    componentDidMount = () => {
        axios.get('/settings').then(res => {
            this.setState({settings: res.data, notification:  res.data.notification})
        })
    };

    toggleNotification = () => {
        axios.put('/settings', {notification: !this.state.notification}).then(res => {
            this.setState({notification:  res.data.notification})
        })
    };

    render() {
        const {classes} = this.props;
        return (
            <>
                <AppBar position="static">
                    <Toolbar disableGutters>
                        <IconButton onClick={this.props.history.goBack} color="inherit">
                            <Icon>chevron_left</Icon>
                        </IconButton>
                        <Typography variant="h6" color="inherit">
                            Settings
                        </Typography>
                    </Toolbar>
                </AppBar>

                <List disablePadding>
                    <ListItem component={Link}  to="/user/history" button className={classes.listItem}>

                        <ListItemText classes={{primary: classes.colorDisabled}}>Notifications</ListItemText>
                        <ListItemSecondaryAction>
                            <IconButton onClick={this.toggleNotification}>
                            {this.state.notification ?
                                <Icon>notifications_active</Icon>: <Icon color={"primary"}>notifications_off</Icon>
                            }
                            </IconButton>

                        </ListItemSecondaryAction>
                    </ListItem>
                    <Divider light component={'li'}/>
                    <ListItem component={this.state.auth ? Link : 'li'} button className={classes.listItem} to="/user/payments">
                        <Icon>system_update</Icon>
                        <ListItemText classes={{primary: classes.colorDisabled}}>Check for Update</ListItemText>
                        <ListItemSecondaryAction>
                            <Icon color={'disabled'}>chevron_right</Icon>
                        </ListItemSecondaryAction>
                    </ListItem>
                    <Divider light component={'li'}/>
                </List>
                <List>
                    <ListItem component={Link} to={"/about"} button className={classes.listItem}>
                        <Icon>info</Icon>
                        <ListItemText classes={{primary: classes.colorDisabled}}>About Us</ListItemText>
                        <ListItemSecondaryAction className={classes.flow}>
                            <Typography variant={"caption"} className={classes.colorDisabled}>01/25/2018</Typography>
                        </ListItemSecondaryAction>
                    </ListItem>
                </List>

            </>
        );
    }
}

Settings.propTypes = {};

export default withStyles(styles)(Settings);
