import React, {Component} from 'react';
import {connect} from 'react-redux';
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import {bindActionCreators} from "redux";
import * as Actions from "../../../store/actions/actions";
import axios from "../../../myaxios"

class BirthdayDialog extends Component {

    state = {
        loading: false,
        value: this.props.user.data.birthday
    };

    onSubmit = (e) => {
        e.preventDefault();
        axios.put("/profile", {birthday: this.state.value}).then(res=>{

            this.props.closeDialog("changeBirthday");
        })
    };

    render() {
        return (
            <Dialog
                open={this.props.dialogs['changeBirthday']}
                onClose={() => this.props.closeDialog("changeBirthday")}
                aria-labelledby="form-dialog-title"
                fullWidth
                className={this.state.loading ? "loading" : ""}
            >
                <form onSubmit={this.onSubmit}>
                    <DialogTitle id="form-dialog-title">Birthday</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Please enter your birthday
                        </DialogContentText>
                        <TextField
                            type={"date"}
                            autoFocus
                            margin="dense"
                            id="birthday"
                            label="Name"
                            fullWidth
                            InputLabelProps={{
                                shrink: true
                            }}
                            value={this.state.value}
                            onChange={(e)=>this.setState({value: e.target.value})}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={()=>this.props.closeDialog('changeBirthday')}>Cancel</Button>
                        <Button type={"submit"}
                                color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        openDialog      : Actions.openDialog,
        closeDialog     : Actions.closeDialog
    }, dispatch);
}
function mapStateToProps({dialogs, user}) {
    return { dialogs, user };
}

export default connect(
    mapStateToProps,mapDispatchToProps
)(BirthdayDialog);
