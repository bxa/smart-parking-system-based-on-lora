import React, {Component} from 'react';

import {withStyles} from "@material-ui/core/styles/index";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Icon from "@material-ui/core/Icon";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction/ListItemSecondaryAction";
import axios from "../../myaxios";

const styles = theme => ({

});
class Payments extends Component {
    state = {
        data: [],
        loading: true
    };

    componentDidMount = () => {
        axios.get('/transactions').then(res => this.setState({data: res.data, loading: false}))
    };

    render() {
        const {classes} = this.props;
        return (
            <>
                <AppBar position="static">
                    <Toolbar disableGutters>
                        <IconButton onClick={this.props.history.goBack} color="inherit">
                            <Icon>chevron_left</Icon>
                        </IconButton>
                        <Typography variant="h6" color="inherit">
                            Payments
                        </Typography>
                    </Toolbar>
                </AppBar>
                {this.state.data.length === 0 ?
                    <Card className={this.state.loading ? "loading " : ""}>
                        <CardContent className={"mt2 mb2"}>
                            <>
                                <Typography variant={"display2"} color={"textSecondary"}  > <Icon className={"textLg mb1"}>error</Icon>  </Typography>
                                <Typography variant={"h6"} color={"textSecondary"}  > Your payments are empty </Typography>
                            </>
                        </CardContent>
                    </Card>
                    :
                    <List className={"bg-white"}>
                        {this.state.data.map((_i, i) =>
                            <ListItem key={i}>
                                <ListItemText
                                    primary={_i.type + " - " + _i.order.duration}
                                    secondary={_i.order.parkingName + " - " +_i.date}
                                />
                                <ListItemSecondaryAction className={"pr-3 text-left"}>
                                    <Typography variant={"h6"} className={"text-blue"}>{_i.amount}</Typography>
                                </ListItemSecondaryAction>
                            </ListItem>
                        )}
                    </List>
                }
            </>
        );
    }

}
Payments.propTypes = {};
export default withStyles(styles)(Payments);
