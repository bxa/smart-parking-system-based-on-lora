import React, {Component} from 'react';

import {withStyles} from "@material-ui/core/styles/index";
import Card from "@material-ui/core/Card";
import CardContent from '@material-ui/core/CardContent';
import classNames from 'classnames';
import Avatar from "@material-ui/core/Avatar";
import profilePic from '../../img/uxceo-128.jpg';
import profileNotLoggedIn from '../../img/profile.png';
import CardHeader from "@material-ui/core/CardHeader";
import Typography from "@material-ui/core/Typography";
import Gray from "@material-ui/core/colors/grey"
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Icon from "@material-ui/core/Icon";
import Link from "react-router-dom/Link";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import {bindActionCreators} from "redux";
import * as Actions from "../../store/actions/actions";
import {connect} from "react-redux";
import axios from "../../myaxios"

const styles = theme => ({
    avatar: {
        margin: 10,
    },
    bigAvatar: {
        width: 90,
        height: 90,
        border: '5px solid rgba(255,255,255,0.4)',
        margin: 'auto'
    },
    card: {
        background: theme.palette.primary.main
    },
    label: {
        color: "white",
        fontSize: 16,
        marginTop: 10
    },
    listItem: {
        paddingTop: 17,
        paddingBottom: 17,
        background: "white"
    },
    colorDisabled: {
        color: Gray["600"],
        fontWeight: 'lighter',
        display: 'inline-block',
        verticalAlign: 'middle',
        marginRight: 7
    },
    iconRight: {
        verticalAlign: 'middle',
    }
});

class MyProfile extends Component {

    state = {
        phone: "",
    };

    handleClickOpenLogin = () => {
        this.props.openDialog("login");
    };

    sendSMS = () => {
        this.setState({loading: true});
        axios.post('/getCode', {mobile: this.state.phone}).then(res => {
            this.setState({loading: false});
            this.props.openDialog("confirmCode");
        })
    };

    isLoggedInHandler = (auth) => {
        if (auth) {
            return (
                <>
                    <Avatar
                        alt="Adelle Charles"
                        src={profilePic}
                        className={classNames(this.props.classes.avatar, this.props.classes.bigAvatar)}
                    />
                    <Typography variant={"subtitle1"} className={"text-center"}>
                        <Button className={this.props.classes.label} component={Link} to="/user/edit">
                        {this.props.user.data.phone} <Icon className={'vm'}>chevron_right</Icon>
                        </Button>
                    </Typography>
                </>
            )
        }
        else {
            return (
                <>
                    <Avatar
                        alt="Adelle Charles"
                        src={profileNotLoggedIn}
                        className={classNames(this.props.classes.avatar, this.props.classes.bigAvatar)}
                    />
                    <Typography  onClick={this.handleClickOpenLogin} variant={"h6"}
                                className={this.props.classes.label + " text-center"}>
                        Login <Icon className={'vm'}>chevron_right</Icon>
                    </Typography>
                    {this.props.dialogs['login'] &&
                    <Dialog
                        open={this.props.dialogs['login']}
                        onClose={() => this.props.closeDialog("login")}
                        aria-labelledby="form-dialog-title"
                        className={this.state.loading ? "loading" : ""}
                    >
                        <DialogTitle id="form-dialog-title">Login</DialogTitle>
                        <DialogContent>
                            <DialogContentText>
                                Please enter your mobile number
                            </DialogContentText>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                label="Phone number"
                                type="number"
                                fullWidth
                                value={this.state.phone}
                                onChange={(e)=>this.setState({phone: e.target.value})}
                            />
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.sendSMS} fullWidth size={"large"}
                                    color="primary">
                                Send SMS
                            </Button>
                        </DialogActions>
                    </Dialog>
                    }
                    {this.props.dialogs['confirmCode'] &&
                    <Dialog
                        open={this.props.dialogs["confirmCode"]}
                        onClose={() => this.props.closeDialog("confirmCode")}
                        aria-labelledby="form-dialog-title"
                    >
                        <DialogContent>
                            <DialogContentText>
                                Enter the code we've sent you
                            </DialogContentText>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="code"
                                label="Confirmation code"
                                type="number"
                                fullWidth
                                value={this.state.code}
                                onChange={(e)=>this.setState({code: e.target.value})}
                                InputProps={{classes: {input: 'big'}}}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={() => this.props.closeDialog("confirmCode")} variant={"text"}
                                    color="default">
                                <Icon>chevron_left</Icon> Change
                            </Button>
                            <Button onClick={() => {this.props.closeDialog("confirmCode"); this.props.closeDialog("login"); this.props.login(this.state.phone, this.state.code)}} variant={"text"}
                                    color="primary">
                                Send
                            </Button>
                        </DialogActions>
                    </Dialog>
                    }
                </>
            )
        }
    };

    render() {
        const {classes} = this.props;
        const {auth, data} = this.props.user;
        return (
            <>
                <Card className={classes.card}>
                    <CardHeader/>
                    <CardContent>
                        {this.isLoggedInHandler(auth)}
                    </CardContent>
                </Card>
                    <List disablePadding>
                        <ListItem onClick={this.handleClickOpenLogin} component={auth ? Link : 'li'}  to="/user/history" button className={classes.listItem}>
                            <Icon>  assessment</Icon>
                            <ListItemText classes={{primary: classes.colorDisabled}}>History</ListItemText>
                            <ListItemSecondaryAction>
                                <Icon color={'disabled'}>chevron_right</Icon>
                            </ListItemSecondaryAction>
                        </ListItem>
                        <Divider light component={'li'}/>
                        <ListItem onClick={this.handleClickOpenLogin } component={auth ? Link : 'li'} button className={classes.listItem} to="/user/payments">
                            <Icon>assignment</Icon>
                            <ListItemText classes={{primary: classes.colorDisabled}}>Payment</ListItemText>
                            <ListItemSecondaryAction>
                                <Icon color={'disabled'}>chevron_right</Icon>
                            </ListItemSecondaryAction>
                        </ListItem>
                        <Divider light component={'li'}/>
                        <ListItem onClick={this.handleClickOpenLogin } component={auth ? Link : 'li'}  to={'/user/vehicles'} button className={classes.listItem}>
                            <Icon>airport_shuttle</Icon>
                            <ListItemText classes={{primary: classes.colorDisabled}}>Cars</ListItemText>
                            <ListItemSecondaryAction>
                                <Typography variant={"caption"} className={classes.colorDisabled}>H14854</Typography>
                            </ListItemSecondaryAction>
                        </ListItem>
                        <Divider light component={'li'}/>
                        <ListItem onClick={this.handleClickOpenLogin } component={auth ? Link : 'li'} button className={classes.listItem}  to="/user/wallet">
                            <Icon>account_balance_wallet</Icon>
                            <ListItemText classes={{primary: classes.colorDisabled}}>Wallet</ListItemText>
                            <ListItemSecondaryAction className={classes.flow}>
                                <Typography variant={"caption"} className={classes.colorDisabled}>$150</Typography>
                                {/*<Icon  color={'disabled'} className={classes.iconRight}>chevron_right</Icon>*/}
                            </ListItemSecondaryAction>
                        </ListItem>
                    </List>

                    <List>
                        <ListItem onClick={this.handleClickOpenLogin } component={auth ? Link : 'li'} button className={classes.listItem}  to="/user/settings">
                            <Icon>settings</Icon>
                            <ListItemText classes={{primary: classes.colorDisabled}}>Settings</ListItemText>
                            <ListItemSecondaryAction className={classes.flow}>
                                <Typography variant={"caption"} className={classes.colorDisabled}>$150</Typography>
                                {/*<Icon  color={'disabled'} className={classes.iconRight}>chevron_right</Icon>*/}
                            </ListItemSecondaryAction>
                        </ListItem>
                    </List>

                {auth &&
                <List>
                    <ListItem onClick={this.props.logout } component={'li'} button className={classes.listItem}>
                        <ListItemText classes={{primary: classes.colorDisabled}}>Logout</ListItemText>
                    </ListItem>
                </List>}
            </>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        login           : Actions.login,
        logout          : Actions.logout,
        openDialog      : Actions.openDialog,
        closeDialog     : Actions.closeDialog
    }, dispatch);
}

function mapStateToProps({user,dialogs}) {
    return {
        dialogs         : dialogs,
        user            : user,
    }
}

export default withStyles(styles)(connect(mapStateToProps,mapDispatchToProps)(MyProfile));
