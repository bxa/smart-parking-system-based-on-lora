import React, {Component} from 'react';

import {withStyles} from "@material-ui/core/styles/index";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Icon from "@material-ui/core/Icon";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction/ListItemSecondaryAction";
import axios from "../../myaxios";

const styles = theme => ({
    // cardContent: {
    //     margin: {
    //
    //     }
    // }
});
class History extends Component {
    state = {
        data: [],
        loading: true
    };

    componentDidMount = () => {
        axios.get('/parking/history').then(res => this.setState({data: res.data, loading: false}))
    };

    render() {
        const {classes} = this.props;
        return (
            <>
                <AppBar position="static">
                    <Toolbar disableGutters>
                        <IconButton onClick={this.props.history.goBack} color="inherit">
                            <Icon>chevron_left</Icon>
                        </IconButton>
                        <Typography variant="h6" color="inherit">
                            History
                        </Typography>
                    </Toolbar>
                </AppBar>

                {this.state.data.length === 0 ?
                    <Card className={this.state.loading ? "loading " : ""}>
                        <CardContent className={"mt2 mb2"}>
                            <>
                                <Typography variant={"display2"} color={"textSecondary"}  > <Icon className={"textLg mb1"}>error</Icon>  </Typography>
                                <Typography variant={"h6"} color={"textSecondary"}  > Your History is empty </Typography>
                            </>
                        </CardContent>
                    </Card>
                    :
                    <List className={"bg-white"}>
                        {this.state.data.map((_i, i) =>
                            <ListItem key={i}>
                                <ListItemText
                                    primary={_i.parkingName}
                                    secondary={_i.address}
                                />
                                <ListItemSecondaryAction className={"pr-3 text-left"}>
                                    <Typography>{_i.date}</Typography>
                                    <p className={"text-blue"}>{_i.duration}</p>
                                </ListItemSecondaryAction>
                            </ListItem>
                        )}
                    </List>
                }

            </>
        );
    }

}
History.propTypes = {};
export default withStyles(styles)(History);
