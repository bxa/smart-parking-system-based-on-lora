import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';

const styles = {
    root: {
        background: 'white',
        borderRadius: 3,
        paddingLeft: 10,
        flexGrow: 1,
    },
    flex: {

        flex: 1,
    },
    searchcolor: {
        color: '#ccc'
    },
    gutters: {
        paddingRight: 5
    }
};

class MainToolbar extends React.Component {
    state = {
        auth: true,
        anchorEl: null,
    };

    handleMenu = event => {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleClose = () => {
        this.setState({ anchorEl: null });
    };

    render() {
        const { classes } = this.props;
        const { auth, anchorEl } = this.state;
        const open = Boolean(anchorEl);

        return (
            <div className={classes.root}>
                <AppBar position="fixed">
                    <Toolbar classes={{gutters: classes.gutters}}>
                        <Input
                            placeholder="Search Here"
                            // className={classes.flex}
                            classes={{
                                root: classes.root,
                                input: classes.input,
                            }}
                            inputProps={{
                                'aria-label': 'Description',
                            }}
                            startAdornment={
                                <InputAdornment position="start">
                                    <Icon className={classes.searchcolor}>search</Icon>
                                </InputAdornment>
                            }
                        />
                        {auth && (
                            <div>
                                <IconButton
                                    aria-owns={open ? 'menu-appbar' : null}
                                    aria-haspopup="true"
                                    onClick={this.handleMenu}
                                    color="inherit"
                                >
                                    <Icon>add</Icon>
                                </IconButton>
                                <Menu
                                    id="menu-appbar"
                                    anchorEl={anchorEl}
                                    anchorOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    open={open}
                                    onClose={this.handleClose}
                                >
                                    <MenuItem onClick={this.handleClose}>Scan QR</MenuItem>
                                    <MenuItem onClick={this.handleClose}>My account</MenuItem>
                                </Menu>
                            </div>
                        )}
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

export default withStyles(styles)(MainToolbar);
