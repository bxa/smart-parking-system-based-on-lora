import React, {Component} from "react";
import {Dimensions, Platform, Text, View} from "react-native";
import {withNavigation} from "react-navigation";
import {connect} from "react-redux";
import Container from "../../UI/Container";
import {QRScannerView} from "react-native-qrcode-scanner-view";
import * as qs from "qs";
import {withTheme} from "styled-components";
import {check, PERMISSIONS, RESULTS} from "react-native-permissions";
// import * as  ChatActions from "../Chat/store/actions/actions";
import {bindActionCreators} from "redux";

// import _ from '../../translation';

class ScanQrCode extends Component {

    state = {
        cameraPermission: false
    };

    componentDidMount() {
        if (Platform.OS === "android") {

            check(PERMISSIONS.ANDROID.CAMERA)
                .then(result => {
                    switch (result) {
                        case RESULTS.GRANTED:
                            this.setState({cameraPermission: true});
                            console.log('The permission is granted');
                            break;
                        case RESULTS.UNAVAILABLE:
                            this.setState({cameraPermission: "This feature is not available (on this device / in this context)"});
                            console.log(
                                'This feature is not available (on this device / in this context)',
                            );
                            break;
                        case RESULTS.DENIED:
                            this.setState({cameraPermission: "The permission has not been requested / is denied but requestable"});
                            console.log(
                                'The permission has not been requested / is denied but requestable',
                            );
                            break;
                        case RESULTS.BLOCKED:
                            this.setState({cameraPermission: "The permission is denied and not requestable anymore"});
                            console.log('The permission is denied and not requestable anymore');
                            break;
                    }
                })
                .catch(error => {
                    // …
                });
        } else {
            check(PERMISSIONS.IOS.CAMERA)
                .then(result => {
                    switch (result) {
                        case RESULTS.GRANTED:
                            this.setState({cameraPermission: true});
                            console.log('The permission is granted');
                            break;
                        case RESULTS.UNAVAILABLE:
                            this.setState({cameraPermission: "UNAVAILABLE"});
                            console.log(
                                'This feature is not available (on this device / in this context)',
                            );
                            break;
                        case RESULTS.DENIED:
                            this.setState({cameraPermission: "DENIED"});
                            console.log(
                                'The permission has not been requested / is denied but requestable',
                            );
                            break;
                        case RESULTS.BLOCKED:
                            this.setState({cameraPermission: "BLOCKED"});
                            console.log('The permission is denied and not requestable anymore');
                            break;
                    }
                })
                .catch(error => {
                    // …
                });
        }
    }

    barcodeReceived = (event) => {
        const params = qs.parse(event.data, {ignoreQueryPrefix: true});
        // if (!!params.userProfileId) {
        //     this.props.setSelectedContactId(params.userProfileId);
        //     this.props.navigation.navigate({routeName: "userProfile"});
        // }
    };

    // todo translate
    render() {
        return (
            <Container>
                <View style={{flex: 1, height: Dimensions.get('window').height}}>
                    <QRScannerView
                        rectStyle={{
                            height: 250,
                            width: 250,
                        }}
                        scanBarAnimateTime={1500}
                        cornerStyle={{
                            borderWidth: 4,
                            borderColor: this.props.theme.colors.text
                        }}
                        onScanResult={this.barcodeReceived}
                        renderHeaderView={() => <Text style={{color: 'white', textAlign: 'center', padding: 16}}>
                            {"Scan qr code"}
                        </Text>}
                        scanBarAnimateReverse={true}/>
                </View>

            </Container>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        // setSelectedContactId: ChatActions.setSelectedContactId,
    }, dispatch);
}

export default connect(null, mapDispatchToProps)(withNavigation(withTheme(ScanQrCode)));
