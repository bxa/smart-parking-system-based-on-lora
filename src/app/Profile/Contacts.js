import React, {useState} from "react";
import {View} from "react-native";
import {NavigationActions, withNavigation} from "react-navigation";
import {useDispatch, useSelector} from "react-redux";
import Input from "../../UI/Input";
import Button from "../../UI/Button";
import axios, {userPrefix} from "../../myaxios";
import * as Actions from "../../store/actions/actions";
import Container from "../../UI/Container";
import Card from "../../UI/Card";
import Tabs from "../../UI/Tabs";
import Tab from "../../UI/Tab";
import {withTheme} from "styled-components";
import ActionsArea from "../../UI/ActionsArea";
import Empty from "../../UI/Empty";

// const ActionsArea = styled.View`
//     position: absolute;
//     bottom: 0;
//     left: 0;
//     width: 100%;
//     height: 50px;
//     background-color: ${({theme})=>theme.colors.bg};
//     flex-direction: row;
//     align-items: center;
//     justify-content: space-around ;
// `;

const Contacts = ({theme}) => {
    const user = useSelector(({user}) => user);
    const dispatch = useDispatch();
    const loading = useSelector(({loading}) => loading);

    const [formData, formDataSet] = useState({
        email: user.contact.email,
        phone: user.contact.phone,
        preset: user.contact.preset,
        wechat: user.contact.info.wechat,
        qq: user.contact.info.qq,
        msn: user.contact.info.msn,
        blog: user.contact.info.blog,
    });

    const onChange = name => val => {
        formDataSet({
            ...formData,
            [name]: val
        })
    };

    const submit = () => {
        dispatch(Actions.startLoading());
        const newFormData = {
            ...formData,
            info: [
                {key: "wechat", value: formData.wechat},
                {key: "qq", value: formData.qq},
                {key: "msn", value: formData.msn},
                {key: "blog", value: formData.blog},
            ]
        };
        axios.put(`${userPrefix}/contact`, formData).then(res => {
            dispatch(Actions.showMessage({
                message: "Information saved!",
            }));
            dispatch(Actions.finishLoading());
            dispatch(Actions.setUserData());
        })
    };

    return (
        <>
            <Container hasActions>
                <Card>
                    <Tabs>
                        <Tab onPress={() => dispatch(NavigationActions.navigate({routeName: 'general'}))}
                             icon={"information-circle"}>General</Tab>
                        <Tab icon={"book"} active>Contact</Tab>
                        <Tab onPress={() => dispatch(NavigationActions.navigate({routeName: 'security'}))}
                             icon={"lock"}>Security</Tab>
                    </Tabs>
                    {loading ? <Empty/> :
                        <View style={{padding: 20}}>
                            <Input
                                label={"email"}
                                value={formData.email}
                                onChangeText={onChange("email")}
                                autoCapitalize="none"
                                keyboardType={"email-address"}
                                autoCorrect={false}/>
                            <Input
                                keyboardType={"phone-pad"}
                                label={"phone"}
                                value={formData.phone}
                                onChangeText={onChange("phone")}
                                autoCapitalize="none"
                                autoCorrect={false}/>
                            <Input
                                label={"preset"}
                                value={formData.preset}
                                onChangeText={onChange("preset")}
                                autoCapitalize="none"
                                autoCorrect={false}/>
                            <Input
                                label={"wechat"}
                                value={formData.wechat}
                                onChangeText={onChange("wechat")}
                                autoCapitalize="none"
                                autoCorrect={false}/>
                            <Input
                                label={"qq"}
                                value={formData.qq}
                                onChangeText={onChange("qq")}
                                autoCapitalize="none"
                                autoCorrect={false}/>
                            <Input
                                label={"msn"}
                                value={formData.msn}
                                onChangeText={onChange("msn")}
                                autoCapitalize="none"
                                autoCorrect={false}/>
                            <Input
                                label={"blog"}
                                value={formData.blog}
                                onChangeText={onChange("blog")}
                                autoCapitalize="none"
                                autoCorrect={false}/>
                        </View>
                    }
                </Card>
            </Container>

            <ActionsArea>
                <Button color={"primary"} onPress={submit}>Send</Button>
            </ActionsArea>
        </>
    )
};

export default withNavigation(withTheme(Contacts));
