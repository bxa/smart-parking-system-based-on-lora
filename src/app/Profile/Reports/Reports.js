import React, {useEffect, useState} from "react";
import {FlatList} from "react-native";
// import Button from "../../../UI/Button";
import {useDispatch} from "react-redux";
import MainHeader from "../../../UI/MainHeader";
import Card from "../../../UI/Card";
import Container from "../../../UI/Container";
import ListItem from "../../../UI/ListItem";
import P from "../../../UI/P";
import {withTheme} from "styled-components";
import Empty from "../../../UI/Empty";
import {finishLoading, startLoading} from "../../../store/actions/loading.action";
import axios, {baseURL} from "../../../myaxios";

const Reports = ({theme}) => {
    const [data, setData] = useState({
        metadata: {
            page: 0,
            totalItems: 0,
            totalPage: 0,
        },
        result: []
    });
    const dispatch = useDispatch();
    useEffect(() => {
        getData();
    }, []);


    const getData = () => {
        dispatch(startLoading());
        axios.get(`${baseURL}/profile/reports`).then(res => {
            dispatch(finishLoading());
            setData(res.data);
        }).catch(er=> {
            console.log(er);
        })
    };
    return (
        <Container onRefresh={getData}>
            <MainHeader
                title={"Transactions"}
                subTitle={`${data.metadata.totalItems} Reports`}
            />
            <Card>
                {data.result.length > 0 ?
                    <FlatList
                        data={data.result}
                        renderItem={({item}) =>
                            <ListItem
                                // button
                                // onPress={() => {
                                //     dispatch(NavigationActions.navigate({
                                //         routeName: 'Lot',
                                //         params: {id: item.spot}
                                //     }))
                                // }}
                                title={item.order.date}
                                actions={<P type={"small"}>{item.date}</P>}
                                subtitle={item.lot.name}
                            />
                        }
                    />
                    :
                    <Empty/>
                }
            </Card>
        </Container>
    );
};

export default withTheme(Reports);
