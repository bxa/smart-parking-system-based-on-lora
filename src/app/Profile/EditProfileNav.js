import React from "react";
import {NavigationActions, withNavigation} from "react-navigation";
import {useDispatch} from "react-redux";
import Tabs from "../../UI/Tabs";
import Tab from "../../UI/Tab";

const EditProfileNav = (props) => {
    const dispatch = useDispatch();
    return (
        <Tabs>
            <Tab onPress={()=>dispatch(NavigationActions.navigate({routeName: 'general'}))} icon={"information-circle"}>General</Tab>
            <Tab icon={"book"}  onPress={()=>dispatch(NavigationActions.navigate({routeName: 'contact'}))}>Contact</Tab>
            <Tab active  icon={"lock"}>Security</Tab>
        </Tabs>
    )
};

export default withNavigation(EditProfileNav);
