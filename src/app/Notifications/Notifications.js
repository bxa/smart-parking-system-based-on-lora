import React, {useEffect, useState} from "react";
import {FlatList} from "react-native";
// import Button from "../../UI/Button";
import {NavigationActions} from "react-navigation";
import {useDispatch} from "react-redux";
import MainHeader from "../../UI/MainHeader";
import Card from "../../UI/Card";
import Container from "../../UI/Container";
import ListItem from "../../UI/ListItem";
import Icon from "react-native-vector-icons/Ionicons";
import {withTheme} from "styled-components";
import Empty from "../../UI/Empty";

const Notifications = ({theme}) => {
    const [data, setData] = useState({content: 0});
    const dispatch = useDispatch();
    useEffect(() => {
        getData();
    }, []);


    const getData = (params) => {
        // dispatch(startLoading());
        // axios.get(`${chatPrefix}/notifications`, {params}).then(res => {
        //     setData(res.data);
        //     dispatch(finishLoading());
        //     if (params)
        //         paramSet(params)
        // })
    };
    return (
        <Container>
            <MainHeader
                title={"Notifications"}
                subTitle={"45 unread"}
            />
            <Card>
                {data.content.length > 0 ?
                    <FlatList
                        data={data.content}
                        renderItem={({item}) =>
                            <ListItem
                                avatar={<Icon color={theme.colors.textLight} size={40}
                                              name={item.priority === 1 ? "ios-alert" : item.priority === 2 ? "ios-information-circle-outline" : "ios-warning"}/>}
                                button
                                onPress={() => {
                                    if (item.sourceType === "task") {
                                        dispatch(NavigationActions.navigate({
                                            routeName: 'task',
                                            params: {id: item.sourceId}
                                        }))
                                    }
                                }}
                                title={item.title}
                                subtitle={item.detail}
                            />
                        }
                    />
                    :
                    <Empty/>
                }
            </Card>
        </Container>
    );
};

export default withTheme(Notifications);
