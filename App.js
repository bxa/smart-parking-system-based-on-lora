import React, {useEffect} from 'react';
import store from "./src/store/store";
import {Provider, connect, useSelector} from "react-redux";
import {themes} from "./src/styles/themes";
import {createReduxContainer} from "react-navigation-redux-helpers";
import {StyleSheet, ScrollView, View, Alert,Text, BackHandler, StatusBar} from "react-native";
import {ThemeProvider} from "styled-components";
import styled from 'styled-components/native';
import {MenuProvider} from 'react-native-popup-menu';
import { NavigationActions } from 'react-navigation';
import Router from "./src/Router";
import changeNavigationBarColor from 'react-native-navigation-bar-color';
import Message from "./src/UI/Message";
console.disableYellowBox = true;
const Root = createReduxContainer(Router);

class navigator extends React.Component {

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

    onBackPress = () => {
        const { nav, dispatch } = this.props;
        if ((nav.index === 0 || nav.index === 1 || nav.index === 2) && (nav.routes[nav.index].index === 0) ) {
            if(nav.routes[2].routes[nav.routes[nav.index].index].index === 0)
                Alert.alert(
                    'Exit App',
                    'Are you sure you want to exit the SPS?',
                    [
                        {
                            onPress: () => BackHandler.exitApp(),
                            text: 'Yes',
                        },
                        {
                            style: 'cancel',
                            text: 'Cancel',
                        },
                    ],
                    { cancelable: false }
                );
        }
        dispatch(NavigationActions.back());
        return true;
    };

    render() {
        const { nav, dispatch } = this.props;

        return <Root state={nav} dispatch={dispatch} />;
    }
}

const mapStateToProps = state => ({
    nav: state.nav,
});

const AppWithNavigationState = connect(mapStateToProps)(navigator);

const MainView = styled.View`
  flex: 1
`;

const Theming = () => {
    const themeName = useSelector(({theme}) => theme);
    useEffect(()=> {
        changeNavigationBarColor(themes[themeName].colors.bg, themeName !== "dark");
    },[themeName]);

    return (
        <ThemeProvider theme={themes[themeName]}>
            <MenuProvider  customStyles={menuProviderStyles}>
                <MainView>
                    <Message />
                    <StatusBar backgroundColor={themes[themeName].colors.bg} barStyle={`${themeName==="dark"?"light":"dark"}-content`}/>
                    <AppWithNavigationState/>
                </MainView>
            </MenuProvider>
        </ThemeProvider>
    )
};

const App = () => {
    return (
        <Provider store={store}>
            <Theming/>
        </Provider>
    );
};

const styles = StyleSheet.create({
    backdrop: {
        backgroundColor: 'black',
        opacity: 0.5,
    },
    anchorStyle: {
        backgroundColor: 'blue',
    },
});

const menuProviderStyles = {
    backdrop: styles.backdrop,
};

export default App;
